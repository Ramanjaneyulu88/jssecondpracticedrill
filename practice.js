

// 1. Find all Web Developers

const findWebDevelopers = (arr) => {
  return arr.filter((each) => {
    if (each["job"].indexOf("Web Developer") !== -1) {
      return true;
    }
  });
};





// 2.Convert all the salary values into proper numbers instead of strings

const convertStringToNumbers = (employeeArray) => {
  return employeeArray.map((each) => {
    each["salary"] = parseFloat(each["salary"].slice(1));
    return each;
  });
};



// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

const correctSalary = (employeeArray) => {
  employeeArray.map((each) => {
    each["corrected_salary"] = each["salary"] * 10000;
  });
};


// 4. Find the sum of all salaries

const sumOfSalaries = (employeeArray) => {
  return employeeArray.reduce((acc, each) => {
    acc += each["corrected_salary"];
    return acc;
  }, 0);
};

//console.log(sumOfSalaries(employeeArray));
// 5. Find the sum of all salaries based on country using only HOF method

const sumOfSalariesOfEachCountry = (employeeArray) => {
  return employeeArray.reduce((acc, eachEmployee) => {
    if (!acc[eachEmployee["location"]]) {
      acc[eachEmployee["location"]] = eachEmployee["corrected_salary"];
    }

    acc[eachEmployee["location"]] += eachEmployee["corrected_salary"];

    return acc;
  }, {});
};

//console.log(sumOfSalariesOfEachCountry(employeeArray));
// 6. Find the average salary of based on country using only HOF method

const averageSalary = (employeeArray) => {
    let totalSalaryObject = employeeArray.reduce((acc, eachEmployee) => {
      if (!acc[eachEmployee["location"]]) {
        acc[eachEmployee["location"]] = {}
        acc[eachEmployee["location"]]["total_salary"] = eachEmployee["corrected_salary"];
        acc[eachEmployee["location"]]["total_count"] = 1
      }
  
      acc[eachEmployee["location"]]["total_salary"] += eachEmployee["corrected_salary"];
      acc[eachEmployee["location"]]["total_count"] += 1
  
      return acc;
    }, {})

    return Object.entries(totalSalaryObject).reduce((acc,eachArr) => {
            acc[eachArr[0]] = eachArr[1].total_salary / eachArr[1].total_count
            return acc
    },{})

  };


 


  module.exports = {
    findWebDevelopers,
    convertStringToNumbers,
    correctSalary,
    sumOfSalaries,
    sumOfSalariesOfEachCountry,
    averageSalary
  }